import pygame as pg
import random
from os import path
from Player import Player
from Platform import Platform
from Spritesheet import Spritesheet
from Colors import *
from Properties import *


class Game:
    def __init__(self):
        pg.init()
        pg.mixer.init()
        pg.display.set_caption(TITLE)
        self.screen = pg.display.set_mode((WIDTH, HEIGHT))
        self.clock = pg.time.Clock()
        self.running = True
        self.playing = False
        self.font_name = pg.font.match_font(FONT_NAME)
        self.load_data()

    def load_data(self):
        # load high score
        self.dir = path.dirname(__file__)
        img_dir = path.join(self.dir, 'resources', 'images')
        self.snd_dir = path.join(self.dir, 'resources', 'sounds')

        fullpath = path.join(self.dir, HIGH_SCORE_FILE)
        file_exist = 'r+' if path.isfile(fullpath) else 'w'
        with open(fullpath, file_exist) as f:
            try:
                self.highscore = int(f.read())
            except:
                self.highscore = 0

        self.spritesheet = Spritesheet(path.join(img_dir, SPRITESHEET))
        self.jump_snd = pg.mixer.Sound(path.join(self.snd_dir, 'Jump7.wav'))

    def new(self):
        self.score = 0
        self.all_sprites = pg.sprite.Group()
        self.platforms = pg.sprite.Group()
        self.player = Player(self)
        self.all_sprites.add(self.player)
        
        for platform in PLATFORM_LIST:
            pl = Platform(self, *platform)
            self.platforms.add(pl)
            self.all_sprites.add(pl)
        
        pg.mixer.music.load(path.join(self.snd_dir, 'HappyTune.wav'))
        self.run()
        
    def run(self):
        pg.mixer.music.play(loops=-1)
        self.playing = True
        while self.playing: 
            self.clock.tick(FPS)
            self.events()
            self.update()
            self.draw()
        pg.mixer.music.fadeout(500)

    def update(self):
        self.all_sprites.update()
        if self.player.vel.y > 0:
            hits = pg.sprite.spritecollide(self.player, self.platforms, False)
            if hits:
                lowest = hits[0]
                for hit in hits:
                    if hit.rect.bottom > lowest.rect.bottom:
                        lowest = hit
                
                if self.player.pos.x > lowest.rect.left - 10 and self.player.pos.x < lowest.rect.right + 10:
                    if self.player.pos.y < lowest.rect.centery:
                        self.player.pos.y = lowest.rect.top
                        self.player.vel.y = 0
                        self.player.jumping = False
        
        # if player enters top 1/4 of screen
        if self.player.rect.top <= HEIGHT / 4:
            self.player.pos.y += max(abs(self.player.vel.y), 2)
            for plat in self.platforms:
                plat.rect.y += max(abs(self.player.vel.y), 2)
                if plat.rect.top >= HEIGHT:
                    plat.kill()
                    self.score += 10

        # Die!
        if self.player.rect.bottom > HEIGHT:
            for sprite in self.all_sprites:
                sprite.rect.y -= max(self.player.vel.y, 10)
                if sprite.rect.bottom < 0:
                    sprite.kill()
        
        if len(self.platforms) == 0:
            self.playing = False

        # Spawn new platforms
        while len(self.platforms) < 6:
            width = random.randrange(50, 100)
            p = Platform(self, random.randrange(0, WIDTH - width), random.randrange(-75, -30))
            self.platforms.add(p)
            self.all_sprites.add(p)

    def events(self):
        for event in pg.event.get():
            if event.type == pg.QUIT:
                if self.playing:
                    self.playing = False
                self.running = False

            if event.type == pg.KEYDOWN:
                if event.key == pg.K_SPACE:
                    self.player.jump()
            
            if event.type == pg.KEYUP:
                if event.key == pg.K_SPACE:
                    self.player.jump_cut()

    def draw(self):
        self.screen.fill(BGCOLOR)
        self.all_sprites.draw(self.screen)
        self.screen.blit(self.player.image, self.player.rect)
        self.drawText(str(self.score), 22, WHITE, WIDTH / 2, 15)
        pg.display.flip()

    def quit(self):
        pass

    def presentScene(self, scene):
        pg.mixer.music.load(path.join(self.snd_dir, 'Yippee.wav'))
        pg.mixer.music.play(loops=-1)
        if scene == SCENE_SPLASH:
            self.splash_scene()
        elif scene == SCENE_GAME_OVER:
            self.gameover_scene()
        pg.mixer.music.fadeout(500)

    def splash_scene(self):
        self.screen.fill(BGCOLOR)
        self.drawText(TITLE, 48, WHITE, WIDTH / 2, HEIGHT / 4)
        self.drawText("Arrows to move, space to jump", 22, WHITE, WIDTH / 2, HEIGHT / 2)
        self.drawText("Press any key to play", 22, WHITE, WIDTH / 2, HEIGHT * 3/4)
        self.drawText("High Score: " + str(self.highscore), 22, WHITE, WIDTH / 2, 15)
        pg.display.flip()
        self.waiting_for_key()

    def gameover_scene(self):
        if not self.running:
            return
        self.screen.fill(BGCOLOR)
        self.drawText("GAME OVER", 48, WHITE, WIDTH / 2, HEIGHT / 4)
        self.drawText("Score: " + str(self.score), 22, WHITE, WIDTH / 2, HEIGHT / 2)
        self.drawText("Press any key to play again", 22, WHITE, WIDTH / 2, HEIGHT * 3/4)
        if self.score > self.highscore:
            self.highscore = self.score
            self.drawText("NEW HIGH SCORE!", 22, WHITE, WIDTH / 2, HEIGHT / 2 + 40)
            with open(path.join(self.dir, HIGH_SCORE_FILE), 'w') as f:
                f.write(str(self.score))
        else:
            self.drawText("High Score: " + str(self.highscore), 22, WHITE, WIDTH / 2, HEIGHT / 2 + 40)

        pg.display.flip()
        self.waiting_for_key()

    def waiting_for_key(self):
        waiting = True
        while waiting:
            self.clock.tick(FPS)
            for event in pg.event.get():
                if event.type == pg.QUIT:
                    waiting = False
                    self.running = False
                if event.type == pg.KEYUP:
                    waiting = False

    def drawText(self, text, size, color, x, y):
        font = pg.font.Font(self.font_name, size)
        text_surface = font.render(text, True, color)
        text_rect = text_surface.get_rect()
        text_rect.midtop = (x,y)
        self.screen.blit(text_surface, text_rect)