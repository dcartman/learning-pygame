import pygame
import random
from Game import Game
from Properties import *

game = Game()
game.presentScene(SCENE_SPLASH)

while game.running:
    game.new()
    game.presentScene(SCENE_GAME_OVER)

pygame.quit()