# Game Setup
TITLE = "Jump Master"
WIDTH = 480
HEIGHT = 600
FPS = 60
FONT_NAME = 'arial'
SCENE_SPLASH = "START"
SCENE_GAME_OVER = "GAMEOVER"
HIGH_SCORE_FILE = "highscore.txt"
SPRITESHEET = "spritesheet_jumper.png"

#Player Properties
PLAYER_ACCELERATION = 0.5
PLAYER_FRICTION = -0.12
PLAYER_GRAVITY = 0.8
PLAYER_JUMP_POWER = 20

# Starting platforms
PLATFORM_LIST = [(0, HEIGHT - 60), 
                 (WIDTH / 2 - 50, HEIGHT * 3 / 4),
                 (125, HEIGHT - 350),
                 (350, 200),
                 (175, 100)]


