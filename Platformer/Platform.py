import pygame as pg
import random
from Colors import *
from Properties import *

class Platform(pg.sprite.Sprite):
    def __init__(self, game, x, y):
        super(Platform, self).__init__()
        self.game = game
        images = [self.game.spritesheet.get_image(0, 288, 380, 94),
                  self.game.spritesheet.get_image(213, 1662, 201, 100)]
        self.image = random.choice(images)
        self.image.set_colorkey(BLACK)
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y

class Powerup(pg.sprite.Sprite):
    def __init__(self, game, platform):
        super(Platform, self).__init__()
        self.game = game
        self.platform = platform
        images = [self.game.spritesheet.get_image(0, 288, 380, 94),
                  self.game.spritesheet.get_image(213, 1662, 201, 100)]
        self.image = random.choice(images)
        self.image.set_colorkey(BLACK)
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y