#! /usr/local/bin/python3
import pygame
from pygame.locals import *
import random
from Colors import *
from Player import Player
from Properties import *

# game initialiazation
pygame.init()
pygame.mixer.init()
screen=pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("My Game")
clock = pygame.time.Clock()
allSprites = pygame.sprite.Group()

# character creation
player = Player()
allSprites.add(player)


while RUNNING:
    # Setup clock
    clock.tick(FPS)

    # Handle Events
    for event in pygame.event.get():
        if event.type == KEYDOWN:
            if event.key == K_ESCAPE:
                RUNNING = False
        elif event.type == QUIT:
            RUNNING = False

    # Update Game
    allSprites.update()

    # Render
    screen.fill(BLACK)
    allSprites.draw(screen)

    # after drawing everything, flip the display
    pygame.display.flip()


# Once we've exited from the Game Loop
# call quit to finalize game.
pygame.quit()