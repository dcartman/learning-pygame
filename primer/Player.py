import pygame
from pygame.locals import *

class Player(pygame.sprite.Sprite):
    def __init__(self):
        super(Player, self).__init__()
        self.image = pygame.image.load("resources/images/jet.png").convert()
        self.image.set_colorkey((255,255,255), RLEACCEL)
        self.rect = self.image.get_rect()

    def update(self, pressedKeys):
        if pressedKeys[K_UP]:
            self.rect.move_ip(0, -5)

        if pressedKeys[K_DOWN]:
            self.rect.move_ip(0, 5)
        
        if pressedKeys[K_LEFT]:
            self.rect.move_ip(-5, 0)
        
        if pressedKeys[K_RIGHT]:
            self.rect.move_ip(5, 0)

        #Keep player on the screen

        if self.rect.left <= 0:
            self.rect.left = 0
        elif self.rect.right >= 800:
            self.rect.right = 800
        
        if self.rect.top <= 0:
            self.rect.top = 0
        if self.rect.bottom >=600:
            self.rect.bottom = 600