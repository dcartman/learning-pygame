#! /usr/local/bin/python3

import pygame
from pygame.locals import *
from Player import Player
from Enemy import Enemy
from Cloud import Cloud
import Utilities as utl

# Initialize pygame
pygame.init()

# create the screen object
screen = pygame.display.set_mode((800, 600))

# Set FPS
FPS = 60
fpsclock = pygame.time.Clock()

# Custom Events
ADDENEMY = pygame.USEREVENT + 1
pygame.time.set_timer(ADDENEMY, 250)

ADDCLOUD = pygame.USEREVENT + 2
pygame.time.set_timer(ADDCLOUD, 1000)

# instantiate our player, who is currently a rectangle
player = Player()

background = pygame.Surface(screen.get_size())
background.fill((0, 0, 0))

enemies = pygame.sprite.Group()
clouds = pygame.sprite.Group()
allSprites = pygame.sprite.Group()
allSprites.add(player)

# Variable to keep the game loop running
running = True

while running:
    # loop through the event queue
    for event in pygame.event.get():
        if event.type == KEYDOWN:
            if event.key == K_ESCAPE:
                running = False
        elif event.type == QUIT:
            running = False
        elif(event.type == ADDENEMY):
            newEnemy = Enemy()
            enemies.add(newEnemy)
            allSprites.add(newEnemy)
        elif(event.type == ADDCLOUD):
            newCloud = Cloud()
            clouds.add(newCloud)
            allSprites.add(newCloud)
    
    screen.blit(background, (0,0))

    pressedKeys = pygame.key.get_pressed()
    player.update(pressedKeys)

    enemies.update()
    clouds.update()

    # Draw the player to the screen
    for entity in allSprites:
        screen.blit(entity.image, entity.rect)

    if pygame.sprite.spritecollideany(player, enemies):
        player.kill()

    # Update the display
    pygame.display.flip()
    fpsclock.tick(FPS)