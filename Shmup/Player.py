import pygame
from pygame.locals import *
from Colors import *
from Properties import *
from Bullet import Bullet
import random
from Pow import Pow

playerImg = pygame.image.load(PLAYER_IMG)
player_mini_img = pygame.transform.scale(playerImg, (25, 19))
player_mini_img.set_colorkey(BLACK)

class Player(pygame.sprite.Sprite):
    def __init__(self, shoot_sound, all_sprites, bullets):
        super(Player, self).__init__()
        self.image = pygame.transform.scale(playerImg.convert(), (50, 38))
        self.image.set_colorkey(BLACK)
        self.rect = self.image.get_rect()
        self.radius = 20
        self.rect.centerx = PLAYER_POSITION_X
        self.rect.bottom = PLAYER_POSITION_Y
        self.speedx = PLAYER_INITIAL_SPEED
        self.speedy = PLAYER_INITIAL_SPEED
        self.shootSound = shoot_sound
        self.shootDelay = PLAYER_SHOOT_DELAY
        self.lastShot = pygame.time.get_ticks()
        self.shield = PLAYER_SHIELD_STRENGTH
        self.allSprites = all_sprites
        self.bullets = bullets
        self.lives = PLAYER_DEFAULT_LIFE_COUNT
        self.hidden = False
        self.hide_timer = pygame.time.get_ticks()
        self.power = PLAYER_DEFAULT_POWER
        self.power_time = pygame.time.get_ticks()

    
    def update(self, pressedKeys):
        # unhide if hidden
        if self.hidden and pygame.time.get_ticks() - self.hide_timer > PLAYER_HIDDEN_TIME:
            self.hidden = False
            self.rect.centerx = PLAYER_POSITION_X
            self.rect.bottom = PLAYER_POSITION_Y
        
        if self.power >= PLAYER_DEFAULT_POWER + POWER_UP_INCREMENT and pygame.time.get_ticks() - self.power_time > POWER_UP_TIME:
            self.power -= POWER_UP_INCREMENT
            self.power_time = pygame.time.get_ticks()

        self.speedx = PLAYER_INITIAL_SPEED
        self.speedy = PLAYER_INITIAL_SPEED
        
        if pressedKeys[K_LEFT]:
            self.speedx = -(PLAYER_MOVE_SPEED)
        if pressedKeys[K_RIGHT]:
            self.speedx = PLAYER_MOVE_SPEED
        if pressedKeys[K_UP]:
            self.speedy = -(PLAYER_MOVE_SPEED)
        if pressedKeys[K_DOWN]:
            self.speedy = PLAYER_MOVE_SPEED
        if pressedKeys[K_SPACE]:
            self.shoot()

        self.rect.x += self.speedx
        self.rect.y += self.speedy

        if self.rect.right > WIDTH:
            self.rect.right = WIDTH
        elif self.rect.left < 0:
            self.rect.left = 0

        if self.rect.bottom > PLAYER_POSITION_Y:
            self.rect.bottom = PLAYER_POSITION_Y
        elif self.rect.top < PLAYER_POSTION_Y_MAX:
            self.rect.top = PLAYER_POSTION_Y_MAX
    
    def shoot(self):
        now = pygame.time.get_ticks()
        if now - self.lastShot > self.shootDelay:
            self.lastShot = now

            if self.power >= PLAYER_DEFAULT_POWER + POWER_UP_INCREMENT:
                bullet1 = Bullet(self.rect.left, self.rect.centery)
                bullet2 = Bullet(self.rect.right, self.rect.centery)
                self.allSprites.add(bullet1)
                self.allSprites.add(bullet2)
                self.bullets.add(bullet1)
                self.bullets.add(bullet2)
                self.shootSound.play()
            else:
                bullet = Bullet(self.rect.centerx, self.rect.top)
                self.allSprites.add(bullet)
                self.bullets.add(bullet)
                self.shootSound.play()

    def hide(self):
        # hide the player temporarily
        self.hidden = True
        self.hide_timer = pygame.time.get_ticks()
        self.rect.center = (PLAYER_POSITION_X, PLAYER_POSITION_Y + 210)

    def death(self):
        self.hide()
        self.lives -= 1
        self.shield = PLAYER_SHIELD_STRENGTH
    
    def powerup(self, hit):
        if hit.type == 'shield':
            self.shield += random.randrange(10, 30)
            if self.shield >= 100:
                self.shield = 100
        if hit.type == 'gun':
            self.power += POWER_UP_INCREMENT
            self.power_time = pygame.time.get_ticks()
        
        hit.playContactSound()