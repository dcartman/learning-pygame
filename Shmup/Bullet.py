import pygame
from pygame.locals import *
from Properties import *
from Colors import *

bulletImg = pygame.image.load(BULLET_IMG)

class Bullet(pygame.sprite.Sprite):
    def __init__(self, x, y):
        super(Bullet, self).__init__()
        self.image = bulletImg.convert()
        self.image.set_colorkey(BLACK)
        self.rect = self.image.get_rect()
        self.rect.bottom = y
        self.rect.centerx = x
        self.speedy = BULLET_SPEED

    def update(self, pressedKeys):
        self.rect.y += self.speedy

        # Kill if bullet moves off top of screen
        if self.rect.bottom < 0:
            self.kill()
