import pygame
from pygame.locals import *
from Properties import *
from Colors import *
import random

pygame.mixer.init()

powerup_images = {}
powerup_images['shield'] = pygame.image.load(POWER_UP_SHIELD)
powerup_images['gun'] = pygame.image.load(POWER_UP_WEAPON)
shield_sound = pygame.mixer.Sound(POWER_UP_SHIELD_SOUND)
weapon_sound = pygame.mixer.Sound(POWER_UP_WEAPON_SOUND)

class Pow(pygame.sprite.Sprite):
    def __init__(self, center):
        super(Pow, self).__init__()
        self.type = random.choice(['shield','gun'])
        self.image = powerup_images[self.type].convert()
        self.image.set_colorkey(BLACK)
        self.rect = self.image.get_rect()
        self.rect.center = center
        self.speedy = POWER_UP_SPEED

    def update(self, pressedKeys):
        self.rect.y += self.speedy

        # Kill if POW moves off bottom of screen
        if self.rect.top > HEIGHT:
            self.kill()

    def playContactSound(self):
        if self.type == 'shield':
            shield_sound.play()
        
        if self.type == 'gun':
            weapon_sound.play()