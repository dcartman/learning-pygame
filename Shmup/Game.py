import pygame
from pygame.locals import *
import random
from Properties import *
from Colors import *
from Player import Player, player_mini_img
from Mob import Mob
from Explosion import Explosion
from Pow import Pow

pygame.init()
pygame.mixer.init()

class Game:
    def __init__(self):
        self.screen = pygame.display.set_mode((WIDTH, HEIGHT))
        self.clock = pygame.time.Clock()
        self.fontName = pygame.font.match_font('arial')
        self.explosionAnimation = {}
        self.gameOver = True
        self.running = True
        self.__setup__()

        # Load game sounds
        self.shootSound = pygame.mixer.Sound(FX_SND_SHOOT)
        self.explosionSounds = []
        for snd in [FX_SND_EXPLOSION_1, FX_SND_EXPLOSION_2]:
            self.explosionSounds.append(pygame.mixer.Sound(snd))

        # Sprites
        self.background = pygame.image.load(BACKGROUND_IMG).convert()
        self.backgroundRect = self.background.get_rect()

        
        self.__initPlayer__()
        self.__setupMeteors__()
        self.__initMeteors__()

        pygame.display.set_caption("My Game")
        pygame.mixer.music.load(FX_MUSIC)
        pygame.mixer.music.set_volume(0.4)
        pygame.mixer.music.play(loops=-1)
    
    def __setup__(self):
        self.score = 0
        self.allSprites = pygame.sprite.Group()
        self.mobs = pygame.sprite.Group()
        self.bullets = pygame.sprite.Group()
        self.powerups = pygame.sprite.Group()
    
    def __initPlayer__(self):
        # character creation
        # TODO: update Player __init__ to receive the game object
        self.player = Player(self.shootSound, self.allSprites, self.bullets)
        self.allSprites.add(self.player)

    def __setupMeteors__(self):
        # Meteor Explosions
        self.explosionAnimation['lg'] = []
        self.explosionAnimation['sm'] = []
        self.explosionAnimation['player'] = []
        for i in range(MOB_IMG_EXPLOSION_COUNT):
            filename = MOB_IMG_EXPLOSION_FILENAME.format(i)
            playerfilename = PLAYER_IMG_EXPLOSION_FILENAME.format(i)

            img = pygame.image.load(MOB_IMG_EXPLOSION_PATH + filename).convert()
            img.set_colorkey(BLACK)
            
            plimg = pygame.image.load(MOB_IMG_EXPLOSION_PATH + playerfilename).convert()
            plimg.set_colorkey(BLACK)

            img_lg = pygame.transform.scale(img, (75, 75))
            img_sm = pygame.transform.scale(img, (32,32))
            
            self.explosionAnimation['lg'].append(img_lg)
            self.explosionAnimation['sm'].append(img_sm)
            self.explosionAnimation['player'].append(plimg)

    def __initMeteors__(self):
        for i in range(MOB_COUNT):
            self.newmob()

    def newmob(self):
        mob = Mob()
        self.allSprites.add(mob)
        self.mobs.add(mob)

    def start(self):
        self.gameOver = False
    
    def update(self):
        # Update Game
        pressedKeys = pygame.key.get_pressed()
        self.allSprites.update(pressedKeys)

    def reset(self):
        self.__setup__()
        self.__initPlayer__()
        self.__initMeteors__()
    
    def end(self):
        print("end the game")

    def quit(self):
        print("quit the game")

    def draw(self):
        self.screen.fill(BLACK)
        self.screen.blit(self.background, self.backgroundRect)
        self.allSprites.draw(self.screen)
        self.drawText(self.screen, str(self.score), 18, WIDTH / 2, 10)
        self.drawShieldBar(self.screen, 5, 5, self.player.shield)
        self.drawLives(self.screen, WIDTH - 100, 5, self.player.lives, player_mini_img)

        # after drawing everything, flip the display
        pygame.display.flip()
    
    def checkCollisions(self):
        self.__mobBulletCollision__()
        self.__playerMobCollision__()
        self.__playerPowerupCollision__()
        if self.player.lives <= 0 and not self.deathExplosion.alive():
            self.gameOver = True
    
    def __mobBulletCollision__(self):
        hits = pygame.sprite.groupcollide(self.mobs, self.bullets, True, True, pygame.sprite.collide_circle)
        for hit in hits:
            self.score += 50 - hit.radius
            random.choice(self.explosionSounds).play()
            expl = Explosion(hit.rect.center, 'lg', self.explosionAnimation)
            self.allSprites.add(expl)
            if random.random() > 0.9:
                pow = Pow(hit.rect.center)
                self.allSprites.add(pow)
                self.powerups.add(pow)
            self.newmob()

    def __playerMobCollision__(self):
        hits = pygame.sprite.spritecollide(self.player, self.mobs, True, pygame.sprite.collide_circle)
        for hit in hits:
            self.player.shield -= hit.radius * 2
            expl = Explosion(hit.rect.center, 'sm', self.explosionAnimation)
            self.allSprites.add(expl)
            self.newmob()
            if self.player.shield <= 0:
                self.deathExplosion = Explosion(self.player.rect.center, 'player', self.explosionAnimation)
                self.allSprites.add(self.deathExplosion)
                self.player.death()

    def __playerPowerupCollision__(self):
        # check to see if player hit a powerup
        hits = pygame.sprite.spritecollide(self.player, self.powerups, True)
        for hit in hits:
            if hit.type == 'shield' or hit.type == 'gun':
                self.player.powerup(hit)     

    def presentGameOverScreen(self):
        print("present the Game over screen")
        self.screen.blit(self.background, self.backgroundRect)
        self.drawText(self.screen, "SHMUP!", 64, WIDTH / 2, HEIGHT / 4)
        self.drawText(self.screen, "Arrow keys move, Space to fire", 22, WIDTH / 2, HEIGHT / 2)
        self.drawText(self.screen, "Press a key to begin", 18, WIDTH / 2, HEIGHT * 3 / 4)
        pygame.display.flip()
        waiting = True
        while waiting:
            self.clock.tick(FPS)
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                if event.type == pygame.KEYUP:
                    waiting = False

    def drawText(self, surf, text, size, x, y):
        font = pygame.font.Font(self.fontName, size)
        text_surface = font.render(text, True, WHITE)
        text_rect = text_surface.get_rect()
        text_rect.midtop = (x,y)
        surf.blit(text_surface, text_rect)
    
    def drawShieldBar(self, surf, x, y, pct):
        if pct < 0:
            pct = 0
        
        fill = (pct / 100) * PLAYER_SHIELD_STATUS_BAR_LENGTH
        outline_rect = pygame.Rect(x, y, PLAYER_SHIELD_STATUS_BAR_LENGTH, PLAYER_SHIELD_STATUS_BAR_HEIGHT)
        fill_rect = pygame.Rect(x, y, fill, PLAYER_SHIELD_STATUS_BAR_HEIGHT)
        pygame.draw.rect(surf, GREEN, fill_rect)
        pygame.draw.rect(surf, WHITE, outline_rect, 2)
    
    def drawLives(self, surf, x, y, lives, img):
        for i in range(lives):
            img_rect = img.get_rect()
            img_rect.x = x + 30 * i
            img_rect.y = y
            surf.blit(img, img_rect)