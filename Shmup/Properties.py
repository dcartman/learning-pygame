
WIDTH, HEIGHT = 480, 600
FPS = 60
RUNNING = True
GAME_OVER = True
PLAYER_WIDTH, PLAYER_HEIGHT = 50, 40
PLAYER_IMG = "resources/SpaceShooterRedux/PNG/playerShip1_orange.png"
PLAYER_IMG_EXPLOSION_FILENAME = "sonicExplosion0{}.png"
PLAYER_INITIAL_SPEED = 0
PLAYER_MOVE_SPEED = 8
PLAYER_POSITION_X = WIDTH / 2
PLAYER_POSITION_Y = HEIGHT - 10
PLAYER_POSTION_Y_MAX = HEIGHT - HEIGHT * 2 / 3
PLAYER_SHIELD_STRENGTH = 100
PLAYER_SHIELD_STATUS_BAR_LENGTH = 100
PLAYER_SHIELD_STATUS_BAR_HEIGHT = 10
PLAYER_SHOOT_DELAY = 500
PLAYER_DEFAULT_LIFE_COUNT = 3
PLAYER_HIDDEN_TIME = 2000
PLAYER_DEFAULT_POWER = 1
POWER_UP_WEAPON = "resources/SpaceShooterRedux/PNG/Power-ups/bolt_gold.png"
POWER_UP_WEAPON_SOUND = "resources/Sounds/Powerup_weapon.wav"
POWER_UP_SHIELD = "resources/SpaceShooterRedux/PNG/Power-ups/shield_gold.png"
POWER_UP_SHIELD_SOUND = "resources/Sounds/Powerup_shield.wav"
POWER_UP_SPEED = 2
POWER_UP_TIME = 5000
POWER_UP_INCREMENT = 1
MOB_WIDTH, MOB_HEIGHT = 30, 40
MOB_IMG_PATH = "resources/SpaceShooterRedux/PNG/Meteors/"
MOB_IMG_EXPLOSION_PATH = "resources/SpaceShooterRedux/PNG/Damage/"
MOB_IMG_EXPLOSION_FILENAME = "regularExplosion0{}.png"
MOB_IMG_EXPLOSION_COUNT = 9
MOB_IMG_LIST = [
    'meteorBrown_big1.png',
    'meteorBrown_med1.png',
    'meteorBrown_med1.png',
    'meteorBrown_med3.png',
    'meteorBrown_small1.png',
    'meteorBrown_small2.png',
    'meteorBrown_tiny1.png']
MOB_SPAWN_Y_MIN = -100
MOB_SPAWN_Y_MAX = -40
MOB_SPAWN_X_MAX = WIDTH - MOB_WIDTH
MOB_SPAWN_X_MIN = 0
MOB_SPEED_MIN_Y = 1
MOB_SPEED_MAX_Y = 8
MOB_SPEED_MIN_X = -3
MOB_SPEED_MAX_X = 3
MOB_INITIAL_ROT = 0
MOB_ROTATION_SPEED_MIN = -8
MOB_ROTATION_SPEED_MAX = 8
MOB_COUNT = 8
BULLET_WIDTH, BULLET_HEIGHT = 10, 20
BULLET_SPEED = -10
BULLET_IMG = "resources/SpaceShooterRedux/PNG/Lasers/laserRed01.png"
BACKGROUND_IMG = "resources/SpaceShooterRedux/Backgrounds/starfield.png"
FONT_PATH = "resources/SpaceShooterRedux/Bonus/"
FONT_NAME = "kenvector_future.ttf"
FX_SND_EXPLOSION_1 = "resources/Sounds/expl3.wav"
FX_SND_EXPLOSION_2 = "resources/Sounds/expl6.wav"
FX_SND_SHOOT = "resources/Sounds/pew.wav"
FX_MUSIC = "resources/Sounds/Music.ogg"
