import pygame
from pygame.locals import *
import random
from Properties import *
from Colors import *

meteor_images = []
for img in MOB_IMG_LIST:
    meteor_images.append(pygame.image.load(MOB_IMG_PATH + img))

class Mob(pygame.sprite.Sprite):
    def __init__(self):
        super(Mob, self).__init__()
        self.image_orig = random.choice(meteor_images).convert()
        self.image_orig.set_colorkey(BLACK)
        self.image = self.image_orig.copy()
        self.rect = self.image.get_rect()
        self.radius = int(self.rect.width * 0.85 / 2)
        pygame.draw.circle(self.image, RED, self.rect.center, self.radius)
        self._resetPosition()
    
    def update(self, pressed):
        self.rect.y += self.speedy
        self.rect.x += self.speedx
        self._rotate()

        if self.rect.y > HEIGHT + 10 or self.rect.left < -25 or self.rect.right > WIDTH + 20:
            self._resetPosition()

    def _rotate(self):
        now = pygame.time.get_ticks()
        if now - self.last_update > 50:
            self.last_update = now
            self.rot = (self.rot + self.rot_speed) % 360
            new_image = pygame.transform.rotate(self.image_orig, self.rot)
            old_center = self.rect.center
            self.image = new_image
            self.rect = self.image.get_rect()
            self.rect.center = old_center

    def _resetPosition(self):
        self.rect.x = random.randrange(MOB_SPAWN_X_MIN, MOB_SPAWN_X_MAX)
        self.rect.y = random.randrange(MOB_SPAWN_Y_MIN, MOB_SPAWN_Y_MAX)
        self.speedy = random.randrange(MOB_SPEED_MIN_Y, MOB_SPEED_MAX_Y)
        self.speedx = random.randrange(MOB_SPEED_MIN_X, MOB_SPEED_MAX_X)
        self.rot = MOB_INITIAL_ROT
        self.rot_speed = random.randrange(MOB_ROTATION_SPEED_MIN, MOB_ROTATION_SPEED_MAX)
        self.last_update = pygame.time.get_ticks()