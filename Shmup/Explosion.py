import pygame
from pygame.locals import *
from Properties import *
from Colors import *

class Explosion(pygame.sprite.Sprite):
    def __init__(self, center, size, explosionAnimation):
        super(Explosion, self).__init__()
        self.size = size
        self.image = explosionAnimation[self.size][0]
        self.rect = self.image.get_rect()
        self.rect.center = center
        self.frame = 0
        self.last_update = pygame.time.get_ticks()
        self.frame_rate = 60
        self.expa = explosionAnimation[self.size]

    def update(self, pressed):
        now = pygame.time.get_ticks()
        if now - self.last_update > self.frame_rate:
            self.last_update = now
            self.frame += 1
            if self.frame == len(self.expa):
                self.kill()
            else:
                center = self.rect.center
                self.image = self.expa[self.frame]
                self.rect = self.image.get_rect()
                self.rect.center = center