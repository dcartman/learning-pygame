# Frozen Jam by tgfcoder <https://twitter.com/tgfcoder> licensed under CC-BY-3
# Art from Kenney.nl

import pygame
from pygame.locals import *
import random
from Game import Game
from Colors import *
from Properties import *


# game initialiazation
game = Game()

while game.running:
    # check if i'm dead
    if game.gameOver:
        print("Game is over")
        game.presentGameOverScreen()
        game.reset()
        game.start()


    # Setup clock
    game.clock.tick(FPS)

    # Handle Events
    for event in pygame.event.get():
        if event.type == KEYDOWN:
            if event.key == K_ESCAPE:
                game.gameOver = True
        elif event.type == QUIT:
            game.running = False

    # Update Game
    game.update()

    # check for collisions
    game.checkCollisions()

    # Render
    game.draw()


# Once we've exited from the Game Loop
# call quit to finalize game.
pygame.quit()